@echo off
echo      HyperOS Bootloader Unlock Bypass Tool
echo                Author: etylix
echo Thank to NekoYuzu (MlgmXyysd) for original works
echo                V1.0-3_20240507
echo ------------------------------------------------
echo Hãy bấm [OK] khi được hỏi
echo Đang chờ thiết bị...
timeout 5 > nul
:ask_adb_perm
libraries\adb kill-server
libraries\adb devices > nul
FOR /F "tokens=* USEBACKQ" %%F IN (`libraries\adb shell getprop ro.product.device`) DO (
SET device=%%F
)
if "%device%"=="" goto ask_adb_perm

echo Thiết bị của bạn là: %device%
echo Đang tải dữ liệu cho %device%...
if %device%=="houji" goto not_supported
if %device%=="shennong" goto not_supported
if %device%=="aurora" goto not_supported

if %device%=="gold" goto not_supported
if %device%=="garnet" goto not_supported
if %device%=="zircon" goto not_supported

if %device%=="vermeer" goto not_supported
if %device%=="manet" goto not_supported
if %device%=="duchamp" goto not_supported

@REM MTK
if %device%=="rembrandt" goto mediatek
if %device%=="corot" goto mediatek
if %device%=="rubens" goto mediatek
if %device%=="matisse" goto mediatek
if %device%=="xaga" goto mediatek
if %device%=="pearl" goto mediatek
if %device%=="sea" goto mediatek
if %device%=="ruby" goto mediatek
if %device%=="pearl" goto mediatek

:snapdragon
echo Vui lòng nhấn [Cài đặt] trên điện thoại khi được hỏi
echo Đang tải dữ liệu...
timeout 2 > nul
curl -s -o s.apk https://dl.etylix.me/HUBL_data/s.apk
echo Đang cài đặt...
libraries\adb install s.apk > nul
del s.apk
goto bypass

:mediatek
echo Vui lòng nhấn [Cài đặt] trên điện thoại khi được hỏi
echo Đang tải dữ liệu...
timeout 2 > nul
curl -s -o s.apk https://dl.etylix.me/HUBL_data/m.apk
echo Đang cài đặt...
libraries\adb install m.apk > nul
del m.apk
goto bypass

:not_supported
echo Thiết bị không hỗ trợ unlock, vui lòng liên hệ dịch vụ.
timeout 5 > nul
goto end

:bypass
php -c "%~dp0./php.ini" "%~dp0./bypass.php"

:end
exit