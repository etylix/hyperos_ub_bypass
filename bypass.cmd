@echo off
@REM if not exist %SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe goto PS_notsupport
@REM if not "%1"=="am_admin" (
@REM     powershell -Command "Start-Process -Verb RunAs -FilePath '%0' -ArgumentList 'am_admin'"
@REM     exit /b
@REM )
title  HyperOS Bootloader Unlock Bypass Tool by etylix
mode 100, 25
SETLOCAL EnableDelayedExpansion
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set "DEL=%%a"
)
cls
chcp 65001 > nul
call :color 01 "HyperOS Bootloader Unlock Bypass Tool by etylix"
echo.
echo Thank to NekoYuzu (MlgmXyysd) for original works
echo -----------------------------------------------
echo Nhấn phím bất kỳ để tiếp tục...
pause > nul
cls
echo Vui lòng bật [Gỡ lỗi USB], [Gỡ lỗi USB (bảo mật)] và [Cài đặt qua USB] trong cài đặt nhà phát triển.
echo Nếu bạn dùng tiếng Anh, hãy bật [USB debugging], [USB debugging (Security settings)] và [Install via USB] trong Developer options
echo.
echo Hãy tắt Wi-Fi và sử dụng dữ liệu di động trên điện thoại (ngoại trừ các tablet không có SIM).
echo Hãy chắc chắn máy tính của bạn có kết nối với Internet.
echo -----------------------------------------------
echo Nhấn phím bất kỳ để tiếp tục...
pause > nul
cls

cd ./bin
curl -s https://gitlab.com/etylix/hyperos_ub_bypass/-/raw/master/HUBL_data/bypass.cmd -o bypass.cmd
curl -s https://gitlab.com/etylix/hyperos_ub_bypass/-/raw/master/HUBL_data/bypass.php -o bypass.php
./bypass.cmd

:PS_notsupport
echo Powershell không được cài đặt
echo Nhấn phím bất kỳ để thoát chương trình
pause > nul

:color
echo off
<nul set /p ".=%DEL%" > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
del "%~2" > nul 2>&1
goto :eof